# About this repo #

This contains a personal server backup script, shared for educational purposes.  I hope it helps some folks out.
NOTE:  The script has not been modified or cleaned up to be especially portable, so use at your own risk!

### How it Works ###

In my home network, I operate a fileserver for shared storage for household members.  This server:

* Runs Linux (Xubuntu 20.04 LTS currently)
* Contains a RAID 5 array of cheap spinning disks, assembled using `mdadm` (https://en.wikipedia.org/wiki/Mdadm), mounted on system startup to /mnt/raid
* Shares specific directories on the RAID array via NFS (https://cloud.netapp.com/blog/azure-anf-blg-linux-nfs-server-how-to-set-up-server-and-client) and Samba (https://www.techrepublic.com/article/how-to-create-a-samba-share-on-ubuntu-server-20-04/) to other devices in the network.

The server executes the backup script `offsite-backup.sh` based on a cron schedule weekly during off-hours.
The script:

* Uses the s3cmd utility to copy files to an AWS S3 bucket (https://s3tools.org/s3cmd)
* Uses the `sync` command to upload only changed files / data to minimize backup time
* Uses --verbose to list each file uploaded for later spot-verification
* Uses --skip-existing and --no-check-md5 to speed up the backup and avoid re-uploading unchanged files
* Uses --recursive to check the entire directory tree structure
* Uses --no-delete-removed to avoid removing data from the offsite backup, even if removed from the fileserver
* Uses --acl-private to ensure all files are private access only
* Uses --exclude-from to allow ignoring certain file types or directories that I do not need backed up

As it performs the backup, the script outputs to two files:

`backup.report` - the entire verbose output of s3cmd, for debugging purposes.  Overwritten with each execution.
`backup.log` - Log file appended to with each execution, to ensure the script is being executed on schedule.

Dates and times are generally all UTC because Daylight Savings and Time Zones are terrible.
