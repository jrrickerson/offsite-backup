#!/bin/bash
LOGFILE="${1:-/dev/null}"
S3CMD="$(which s3cmd)"
echo "[`date -Ins`] Backup started." | tee -a "$LOGFILE"
cd /mnt/raid/
echo "[`date -Ins`] Backing up reddog files" | tee -a "$LOGFILE"
$S3CMD -c /home/reddog/.s3cfg sync --verbose --skip-existing --no-check-md5 --recursive --no-delete-removed --acl-private --exclude-from=/mnt/raid/reddog/.s3ignore reddog s3://alexandria-offsite
echo "[`date -Ins`] Backup of reddog files complete. (exit_status=$?)" | tee -a "$LOGFILE"

echo "[`date -Ins`] Backing up stormie files" | tee -a "$LOGFILE"
$S3CMD -c /home/reddog/.s3cfg sync --verbose --skip-existing --no-check-md5 --recursive --no-delete-removed --acl-private --exclude-from=/mnt/raid/stormie/.s3ignore stormie s3://alexandria-offsite
echo "[`date -Ins`] Backup of stormie files complete. (exit_status=$?)" | tee -a "$LOGFILE"
echo "[`date -Ins`] Backup finished." | tee -a "$LOGFILE"
